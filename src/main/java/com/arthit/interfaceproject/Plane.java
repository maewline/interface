/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.interfaceproject;

/**
 *
 * @author Arthit
 */
public class Plane extends Vahicle implements Flyable ,Runable{

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: startEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: stopEngine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane: raiseSpeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane: applyBreak");
    }

    @Override
    public void fly() {
        System.out.println("Plane: Fly");
    }

    @Override
    public void run() {
        System.out.println("Plane: Run");
    }
    
}
