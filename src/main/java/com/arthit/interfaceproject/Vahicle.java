/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.interfaceproject;

/**
 *
 * @author Arthit
 */
public abstract class Vahicle {
    private String engine;

    public Vahicle(String engine) {
        this.engine = engine;
    }
    
    public abstract void startEngine();
    public abstract void stopEngine();
    public abstract void raiseSpeed();
    public abstract void applyBreak();
}
