/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.interfaceproject;

/**
 *
 * @author Arthit
 */
public class Bird extends Poultry {

    private String nickname;

    public Bird(String name) {
        super("Bird", 2);
        this.nickname=name;
    }

   @Override
    public void fly() {
        System.out.println("Bird name: " + nickname + " Fly.");
    }

    @Override
    public void eat() {
        System.out.println("Bird name: " + nickname + " eat.");
    }

    @Override
    public void walk() {
        System.out.println("Bird name: " + nickname + " walk.");
    }

    @Override
    public void speak() {
        System.out.println("Bird name: " + nickname + " can't speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Bird name: " + nickname + " sleep.");
    }

    @Override
    public String toString() {
        return "Animal nickname: " + nickname + super.toString();
    }


}
