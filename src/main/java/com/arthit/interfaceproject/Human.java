/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.interfaceproject;

/**
 *
 * @author Arthit
 */
public class Human extends LandAnimal {

    private String nickname;

    public Human(String name) {
        super("Human", 2);
        this.nickname = name;
    }


    @Override
    public void eat() {
        System.out.println("Human name : " + nickname + " eat.");

    }

    @Override
    public void walk() {
        System.out.println("Human name : " + nickname + " walk.");
    }

    @Override
    public void speak() {
        System.out.println("Human name : " + nickname + " speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Human name : " + nickname + " sleep.");

    }

    @Override
    public String toString() {
        return "Animal nickname: " + nickname + super.toString();
    }
    
    @Override
    public void run() {
        System.out.println("Human name : " + nickname + " run.");
    }
}
