/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.interfaceproject;

/**
 *
 * @author Arthit
 */
public class Bat extends Poultry {

    private String nickname;

    public Bat(String name) {
        super("Bat", 2);
        this.nickname = name;
    }

    @Override
    public void fly() {
        System.out.println("Bat name: " + nickname + " Fly.");
    }

    @Override
    public void eat() {
        System.out.println("Bat name: " + nickname + " eat.");
    }

    @Override
    public void walk() {
        System.out.println("Bat name: " + nickname + " walk.");
    }

    @Override
    public void speak() {
        System.out.println("Bat name: " + nickname + " can't speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Bat name: " + nickname + " sleep.");
    }

    @Override
    public String toString() {
        return "Animal nickname: " + nickname + super.toString();
    }

}
