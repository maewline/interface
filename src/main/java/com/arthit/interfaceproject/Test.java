/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.interfaceproject;

/**
 *
 * @author Arthit
 */
public class Test {

    public static void main(String[] args) {
        Bat bat = new Bat("Banana");
        Plane plane = new Plane("Engine Number 1");
        bat.fly();
        plane.fly();

        Car car = new Car("Engine Number 2");
        Dog dog = new Dog("Toob");
        car.run();
        dog.run();

        Cat cat = new Cat("mewline");
        cat.run();

        Human human = new Human("Arthit");
        human.run();

        Bird bird = new Bird("Kaew");
        bird.fly();

        LoopTest_Text();

        Runable[] runable = {cat, car, human,dog ,plane};
        Flyable[] flyable = {plane, bird, bat};

        System.out.println("Runable Group");
        for (int i = 0; i < runable.length; i++) {
            if (runable[i] instanceof Plane) {
                ((Plane) (runable[i])).startEngine();
                ((Plane) (runable[i])).raiseSpeed();
                ((Plane) (runable[i])).run();
                ((Plane) (runable[i])).fly();

            } else {
                runable[i].run();
            }
        }
        System.out.println("...........................................");
        System.out.println("Flyable Group");
        for (int i = 0; i < flyable.length; i++) {
            if (flyable[i] instanceof Plane) {
                ((Plane) (flyable[i])).startEngine();
                ((Plane) (flyable[i])).raiseSpeed();
                ((Plane) (flyable[i])).run();
                ((Plane) (flyable[i])).fly();

            } else {
                flyable[i].fly();
            }
        }

    }

    private static void LoopTest_Text() {
        System.out.println("");
        System.out.println("...........................................");
        System.out.println("Loop Test");
    }
}
